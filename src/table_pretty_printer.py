def table_pprint(table, header=None):
    widths = list()
    
    if header:
        table = [header] + table
        
    for row in table:
        for index, column in enumerate(row):
            column_width = len(str(column))
            
            if len(widths) <= index:
                widths.append(column_width)
            elif column_width > widths[index]:
                widths[index] = column_width
                
    row_separator = "+" + "+".join(["-" * width for width in widths]) + "+"
    print(row_separator)
    
    def print_row(row):
        row_text = "|"
        for index, column in enumerate(row):
            column = str(column)
            row_text += column + " " * (widths[index] - len(column)) + "|"
        print(row_text)
    
    if header:
        print_row(table[0])
        print(row_separator)
        
        table = table[1:]
    
    for row in table:
        print_row(row)
        
    print(row_separator)
