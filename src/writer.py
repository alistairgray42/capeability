import requests
import bs4
import re
import sqlite3

"""
Scrapes the content of the UCSD CAPEs page and stores the contents in a SQLite database.
"""

base_url = "http://cape.ucsd.edu/responses/Results.aspx"
# must spoof user agent; won't give result if we don't
user_agent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_2) AppleWebKit/537.36 (KHTML,  like Gecko) Chrome/64.0.3282.186 Safari/537.36"

# to the best of my knowledge, these are all the undergraduate course codes
course_codes = {"AIP", "AWP", "ANBI", "ANAR", "ANTH", "ANSC", "BENG", "BIEB", "BICD", "BIPN", "BIBC", "BILD", "BIMM",
                "BISP", "CENG", "CHEM", "CHIN", "COGS", "COMM", "CSE", "CGS", "CAT", "TDDM", "TDHD", "TDMV", "TDPF",
                "TDTR", "DSC", "DSGN", "DOC", "ECON", "EDS", "ERC", "ECE", "ENG", "ENVR", "ESYS", "ETHN", "FMPH",
                "FPM", "FILM", "GLBH", "HITO", "HIAF", "HIEA", "HIEU", "HILA", "HISC", "HINE", "HIUS", "HILD", "HDP",
                "HMNR", "HUM", "INTL", "JAPN", "JUDA", "LATI", "LIAB", "LIGN", "LIGM", "LIHL", "LIIT", "LIPO",
                "LISP", "LTCH", "LTCS", "LTEU", "LTFR", "LTGM", "LTGK", "LTIT", "LTKO", "LTLA", "LTRU", "LTSP",
                "LTTH", "LTWR", "LTEN", "LTWL", "LTEA", "MMW", "MATH", "MAE", "MUIR", "MUS", "NANO", "PHIL", "PHYS",
                "POLI", "PSYC", "MGT", "RELI", "REV", "SIO",  "SOCE", "SOCI", "SE", "TDAC", "TDDE", "TDDR", "TDGE",
                "TDHT", "TDPW", "TDPR", "TWS", "TMC", "USP", "VIS", "WARR", "WCWP"}

total = 0


def extract_dept_code(line):
    return line[:line.find("-") - 1].strip()


def extract_course_code(line):
    # Removes section letter code at end of title, if applicable
    if re.match(" \(.\)", line[-4:]):
        line = line[:-4]
    
    return line.replace("  ", " ").strip()


connection = sqlite3.connect("capes.csv")
cursor = connection.cursor()

# NULL is auto-increment id; others are substituted in
query = "INSERT INTO capes VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?)"

for code in course_codes:
    response = requests.get(base_url, params={"courseNumber": code},  headers={"user-agent": user_agent})
    soup = bs4.BeautifulSoup(response.text,  "html.parser")
    size = 0
    for row in soup.find_all("tr")[1:]:
        # remove newlines
        cells = [e.text.replace("\n", "") for e in row.find_all("td")[:7]]
        
        # add two columns to the beginning
        cells = [extract_dept_code(cells[1]), extract_course_code(cells[1]), *cells]
        
        # cells are: department code, course code, instructor name, course name, term, students enrolled,
        # evaluations made, percentage recommending class, percentage recommending instructor
        
        # if actually belongs to the right department
        if cells[0] == code:
            cursor.execute(query, cells)
            size += 1
            
    print("Finished {} with {} entries!".format(code, size))
    total += size
    
print("Finished all with {} entries!".format(total))
