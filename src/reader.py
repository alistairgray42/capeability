import sqlite3
from src.table_pretty_printer import table_pprint

# Goal: API to filter+sort by all the things you could conceivably want to filter/sort by
# Filter: By department, by course number, by term, by professor
# Sort: By number enrolled, by number evaluating, by enrolled/evaluating ratio, by course approval, by professor
# approval

connection = sqlite3.connect("capes.db")
cursor = connection.cursor()


def query(filters=None, grouping=None):
    """
    Query the SQLite database based on the given parameters, and (temporarily) pretty-print the results.
    :param filters: A dictionary where the keys are field names and the values are the required values
    :param grouping: A set of all fields to group results by
    :return:
    """
    filters = {key: value for key, value in filters.items() if value}
    
    query = "SELECT * FROM CAPES "
    if filters:
        query += "WHERE "
        where_clauses = []
        for key, value in filters.items():
            where_clauses.append('{}="{}" '.format(key, value))
        for clause in where_clauses[:-1]:
            query += clause + "AND "
        query += where_clauses[-1]
        
    if grouping:
        query += "GROUP BY "
        group_clauses = list(grouping)
        for clause in group_clauses[:-1]:
            query += clause + ", "
        query += group_clauses[-1]
        
    print(query)
    cursor.execute(query)
    table_pprint(cursor.fetchall())
    
    
query(filters={"short_title": "CSE 12"}, grouping={"term"})
